mac_app_store
=========

Wrapper role for Jeff Gerlings mas role https://github.com/geerlingguy/ansible-collection-mac

Requirements
------------

Homebrew and mas must be installed. 
If mas is not installed, geerlingguy.mac.mas will install it.
https://github.com/geerlingguy/ansible-collection-mac

Role Variables
--------------

| Variable                    | Required | Description                         | Default |
|-----------------------------|----------|-------------------------------------|---------|
| ibox_mac_store_apps_present | false    | List of apps (id, name) to install. | []      |

Dependencies
------------

```yaml
collections:
  - name: geerlingguy.mac
    version: 2.1.3
```

Example Playbook
----------------
Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.mac_app_store.git
    version: stable
    scm: git
    name: ibox.mac_app_store
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.mac_app_store"
      ansible.builtin.include_role:
        name: "ibox.mac_app_store"
```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
